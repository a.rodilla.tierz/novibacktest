const ComandInfo = require('./../src/controllers/ComandInfo.js');
const ManagerRestoreItems = require('./../src/services/ManagerRestoreItems.js');
const Mongopool = require('./../src/infrastructure/dbpool/mongopool.js');
const ColeccionsRef = require('./../src/infrastructure/emuns/ColeccionsRef.js');
const dotenv = require('dotenv');
dotenv.config({path: '.env'});
const SetReadyDatas = require('./../src/bussinesslogic/SetReadyDatas.js');

const workMongoClient = new Mongopool();

test('happy path restore Items collection', async () => {
  await workMongoClient.removeCollection(ColeccionsRef.Items, {});
  await workMongoClient.removeCollection(ColeccionsRef.LogicPrice, {});
  await SetReadyDatas.checkItemsProcess(workMongoClient);
  await SetReadyDatas.checkLogicProcess(workMongoClient);
  const pileComand = ['tshirt','tshirt'];
  const resp2 = await ComandInfo.SetComand(workMongoClient, pileComand);
  expect(resp2).toBe(40);
});
