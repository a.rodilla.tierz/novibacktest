const ComandInfo = require('./../src/controllers/ComandInfo.js');
const Mongopool = require('./../src/infrastructure/dbpool/mongopool.js');
const dotenv = require('dotenv');
dotenv.config({path: '.env'});

const workMongoClient = new Mongopool();

test('happy path command tshirt', async () => {
  const pileComand = ['tshirt'];
  const resp = await ComandInfo.SetComand(workMongoClient, pileComand);
  expect(resp).toBe(20.0);

  const pileComand2 = ['tshirt','tshirt','tshirt','tshirt'];
  const resp2 = await ComandInfo.SetComand(workMongoClient, pileComand2);
  expect(resp2).toBe((19.0 * 4));

  const pileComand3 = [];
  const resp3 = await ComandInfo.SetComand(workMongoClient, pileComand3);
  expect(resp3).toBe(0.0);
});


test('happy path command voucher', async () => {
  const pileComand = ['voucher'];
  const resp = await ComandInfo.SetComand(workMongoClient, pileComand);
  expect(resp).toBe(5.0);

  const pileComand2 = ['voucher','voucher','voucher','voucher'];
  const resp2 = await ComandInfo.SetComand(workMongoClient, pileComand2);
  expect(resp2).toBe(10.0);

  const pileComand3 = '';
  const resp3 = await ComandInfo.SetComand(workMongoClient, pileComand3);
  expect(resp3).toBe(0.0);
});

test('happy path command mug', async () => {
  const pileComand = ['mug'];
  const resp = await ComandInfo.SetComand(workMongoClient, pileComand);
  expect(resp).toBe(7.5);

  const pileComand2 = ['mug','mug','mug','mug'];
  const resp2 = await ComandInfo.SetComand(workMongoClient, pileComand2);
  expect(resp2).toBe((7.5 * 4));

  const pileComand3 = [];
  const resp3 = await ComandInfo.SetComand(workMongoClient, pileComand3);
  expect(resp3).toBe(0.0);
});

test('happy path command mixed', async () => {
  const pileComand = ['mug','tshirt','tshirt','voucher'];
  const resp = await ComandInfo.SetComand(workMongoClient, pileComand);
  expect(resp).toBe((7.5 + 40.0 + 5.0));

  const pileComand2 = ['mug','tshirt','tshirt','tshirt','voucher'];
  const resp2 = await ComandInfo.SetComand(workMongoClient, pileComand2);
  expect(resp2).toBe((7.5 + (19.0 * 3) + 5.0));

  const pileComand3 = ['mug','tshirt','tshirt','tshirt','voucher','voucher'];
  const resp3 = await ComandInfo.SetComand(workMongoClient, pileComand3);
  expect(resp3).toBe((7.5 + (19.0 * 3) + 5.0));

  const pileComand4 = ['mug','tshirt','tshirt','tshirt','voucher','voucher','voucher'];
  const resp4 = await ComandInfo.SetComand(workMongoClient, pileComand4);
  expect(resp4).toBe((7.5 + (19.0 * 3) + 10.0));
});

test('command mixed with errors', async () => {
  const pileComand = ['mug','tshirt','tshirt','voucher', 'foo'];
  const resp = await ComandInfo.SetComand(workMongoClient, pileComand);
  expect(resp).toBe((7.5 + 40.0 + 5.0));

  const pileComandx = ['mug','tshirt','tshirt','voucher', 15];
  const respx2 = await ComandInfo.SetComand(workMongoClient, pileComandx);
  expect(respx2).toBe((7.5 + 40.0 + 5.0));

  const pileComand2 = 'mug,tshirt,tshirt,tshirt,voucher';
  const resp2 = await ComandInfo.SetComand(workMongoClient, pileComand2);
  expect(resp2).toBe((7.5 + (19.0 * 3) + 5.0));

  const pileComand3 = ['mug','tshirt','tshirt','tshirt','voucher','voucher', undefined];
  const resp3 = await ComandInfo.SetComand(workMongoClient, pileComand3);
  expect(resp3).toBe((7.5 + (19.0 * 3) + 5.0));

  const pileComand4 = ['mug','tshirt','tshirt','tshirt','voucher','voucher','voucher', ['voucher']];
  const resp4 = await ComandInfo.SetComand(workMongoClient, pileComand4);
  expect(resp4).toBe((7.5 + (19.0 * 3) + 10.0));

  const pileComand5 = 777;
  const resp5 = await ComandInfo.SetComand(workMongoClient, pileComand5);
  expect(resp5).toBe(0.0);
});
