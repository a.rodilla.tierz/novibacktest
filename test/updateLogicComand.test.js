const ComandInfo = require('./../src/controllers/ComandInfo.js');
const ManagerLogicPrices = require('./../src/services/ManagerLogicPrices.js');
const Mongopool = require('./../src/infrastructure/dbpool/mongopool.js');
const dotenv = require('dotenv');
dotenv.config({path: '.env'});

const workMongoClient = new Mongopool();

test('happy path command tshirt', async () => {
  const workerLogicManager = new ManagerLogicPrices(workMongoClient);
  await workerLogicManager.updateLogicPriceCollection();
  const pileComand = ['tshirt','tshirt','tshirt','tshirt'];
  const resp2 = await ComandInfo.SetComand(workMongoClient, pileComand);
  expect(resp2).toBe((19.0 * 4));

  const newRules = {
                      TSHIRT: {
                        rule: "> 5",
                        price: "25.00"
                      }
                    };
  await workerLogicManager.updateLogicPriceCollection(newRules);
  const pileComand2 = ['tshirt','tshirt','tshirt','tshirt'];
  const resp3 = await ComandInfo.SetComand(workMongoClient, pileComand2);
  expect(resp3).toBe((20.0 * 4));

  const pileComand3 = ['tshirt','tshirt','tshirt','tshirt','tshirt','tshirt'];
  const resp4 = await ComandInfo.SetComand(workMongoClient, pileComand3);
  expect(resp4).toBe((25.0 * 6));

  await workerLogicManager.updateLogicPriceCollection();
  const pileComand4 = ['tshirt','tshirt','tshirt','tshirt'];
  const resp5 = await ComandInfo.SetComand(workMongoClient, pileComand4);
  expect(resp5).toBe((19.0 * 4));
  const pileComand5 = ['tshirt','tshirt','tshirt','tshirt','tshirt','tshirt'];
  const resp6 = await ComandInfo.SetComand(workMongoClient, pileComand5);
  expect(resp6).toBe((19.0 * 6));
});
