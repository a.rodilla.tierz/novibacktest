// update prices of items about the discount logics
exports.applyLogicPriceItem = (nodeItem) => {
  const margin = nodeItem.logicPrices ? nodeItem.logicPrices.rule : 1;
  if (margin === 1) {
    return notDiscountApply(nodeItem);
  } else if (margin[0] === '>') {
    return updatePriceOverAmount(nodeItem, parseInt(margin.split(' ')[1], 10));
  } else {
    return updatePriceByAgrupations(nodeItem, parseInt(margin, 10));
  }
};

const notDiscountApply = (nodeItem) => {
  return nodeItem.amount * nodeItem.price
};

const updatePriceOverAmount = (nodeItem, pointToApply) => {
  return nodeItem.amount > pointToApply ?
                nodeItem.amount * parseFloat(nodeItem.logicPrices.price)
                : nodeItem.amount * nodeItem.price;
};

const updatePriceByAgrupations = (nodeItem, groupSize) => {
  const discountedPile = parseInt((nodeItem.amount / groupSize), 10);
  const rawPile = parseInt((nodeItem.amount % groupSize), 10);
  return (discountedPile * (nodeItem.price * nodeItem.logicPrices.price)) + (rawPile * nodeItem.price);
};
