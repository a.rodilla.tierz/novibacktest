// I haven't datas on mongodb, load a data sample from .json files at: mediaContent/*.json
const ManagerRestoreItems = require('./../services/ManagerRestoreItems.js');
const ManagerLogicPrices = require('./../services/ManagerLogicPrices.js');

exports.checkItemsProcess = async(clientMongo) => {
  const workerItems = new ManagerRestoreItems(clientMongo);
  if (await workerItems.checkItems() === 0) {
    await workerItems.updateItemsCollection();
  }
};

exports.checkLogicProcess = async(clientMongo) => {
  const workerLogic = new ManagerLogicPrices(clientMongo);
  if (await workerLogic.checkRules() === 0) {
    await workerLogic.updateLogicPriceCollection();
  }
};
