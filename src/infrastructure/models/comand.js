const Product = require('./product.js');
const CheckLogicPrices = require('./../../bussinesslogic/checkLogicPrices.js');

const comand = class {
  constructor() {
    this.items = [];
    this.priceTotal = 0.0;
  }

  async addNewItem(dbObject) {
    if (dbObject && dbObject.code) {
      this.items.push(new Product(dbObject));
    }
  }

  findSelfItem(nodeList) {
    return this.items.findIndex((nodeItems) => {
      return nodeItems.code === nodeList.toUpperCase();
    });
  }

  updateItemAmount(indexItem) {
    this.items[indexItem].updateAmount();
  }

  updateItemLogic(indexItem, ruler) {
    if (ruler && ruler.item && this.items[indexItem]) {
      this.items[indexItem].updateLogicPrices(ruler);
    }
  }

  async determinateComandPrize() {
    this.priceTotal = this.items.reduce((acc, nodeItem) => {
                        return acc + CheckLogicPrices.applyLogicPriceItem(nodeItem);
                      }, 0.0);
  }
}

module.exports = comand;
