const Product = class {
  constructor(dbObject) {
    this.code = dbObject.code || undefined;
    this.name = dbObject.name || undefined;
    this.price = dbObject.price || undefined;
    this.amount = 1;
    this.logicPrices = undefined;
  }

  updateLogicPrices(ruler) {
    if (this.code === ruler.item) {
      this.logicPrices = {
        rule: ruler.rule,
        price: ruler.price
      }
    }
  }

  updateAmount() {
    this.amount++;
  }
}

module.exports = Product;
