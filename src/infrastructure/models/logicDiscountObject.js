const LogicDiscount = class {
  constructor(item, rulerUse) {
    this.item = item;
    this.rule = rulerUse[item].rule;
    this.price = rulerUse[item].price;
  }
}

module.exports = LogicDiscount;
