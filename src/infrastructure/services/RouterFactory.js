const express = require('express');
const apiController = require('./api.js');
const serviceMidddleware = require('./serviceMidddleware.js');

exports.RouterFactory = (router) => {
  router.use(serviceMidddleware.easyVerify);
  // router.get('/setCommand', apiController.setCommand);
  router.post('/setCommand', apiController.setCommand);
  return router;
};
