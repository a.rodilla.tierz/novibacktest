const ComandInfo = require('./../../controllers/ComandInfo.js');

exports.setCommand = async (req, res) => {
  const resp = await ComandInfo.SetComand(req.app.get('clientMongo'), req.body.comandlist);
  res.status(200).send({
    status: 'OK',
    listShopping: resp.items,
    totalPrice: resp.priceTotal
  });
};
