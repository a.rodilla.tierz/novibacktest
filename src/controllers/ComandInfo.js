const ManageComands = require('./../services/ManageComands.js');

exports.SetComand = async (clientMongo, listItem) => {
  const comandObject = new ManageComands(clientMongo);
  return buildResponse(await comandObject.manageTotalPriceOfComand(validateParam(listItem)));
};

const validateParam = (listItem) => {
  if (!listItem || listItem.length === 0) {
    return [];
  } else if (Array.isArray(listItem)) {
    return listItem;
  } else if (isNaN(listItem)) {
    return listItem.split(',');
  }
  return [];
}

const buildResponse = (objResp) => {
  return {
    items: objResp.items.map((nodeItem) => {
      return {
        item: nodeItem.name,
        amount: nodeItem.amount
      };
    }),
    priceTotal: objResp.priceTotal
  }
}
