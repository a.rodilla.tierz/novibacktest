const itemsBase = require('./../../mediaContent/baseItemsShop.json');
const ColeccionsRef = require('./../infrastructure/emuns/ColeccionsRef.js');

const ManagerRestoreItems = class {
  constructor(clientMongo) {
    this.clientMongo = clientMongo;
  }

  async updateItemsCollection() {
    await this.clientMongo.removeCollection(ColeccionsRef.Items, {});
    return await this.clientMongo.insertInCollection(ColeccionsRef.Items, itemsBase);
  }

  async checkItems() {
    return await this.clientMongo.counterCollection(ColeccionsRef.Items, {});
  }
}

module.exports = ManagerRestoreItems;
