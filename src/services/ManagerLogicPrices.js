const Comand = require('./../infrastructure/models/comand.js');
const LogicDiscount = require('./../infrastructure/models/logicDiscountObject.js');
const ColeccionsRef = require('./../infrastructure/emuns/ColeccionsRef.js');
const ruler = require('./../../mediaContent/discountsLogic.json');

const ManagerLogicPrices = class {
  constructor(clientMongo) {
    this.clientMongo = clientMongo;
  }

  async updateLogicPriceCollection(newLogic) {
    const usableLogic = newLogic ? this.formaterRules(newLogic) : this.formaterRules(ruler);
    await this.clientMongo.removeCollection(ColeccionsRef.LogicPrice, {});
    return await this.clientMongo.insertInCollection(ColeccionsRef.LogicPrice, usableLogic);
  }

  formaterRules(rulerToFormat) {
    return Object.keys(rulerToFormat).reduce((acc, nodeRule) => {
                                      acc.push(new LogicDiscount(nodeRule, rulerToFormat));
                                      return acc;
                                    }, []);
  }

  async findRulesPrice(nodeList) {
    return await this.clientMongo.queryToCollection(ColeccionsRef.LogicPrice, {
                        item: nodeList.toUpperCase()
                      });
  }

  async checkRules() {
    return await this.clientMongo.counterCollection(ColeccionsRef.LogicPrice, {});
  }

}

module.exports = ManagerLogicPrices;
