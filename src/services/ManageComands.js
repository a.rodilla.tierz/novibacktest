const Comand = require('./../infrastructure/models/comand.js');
const ManagerLogicPrices = require('./ManagerLogicPrices.js');
const ColeccionsRef = require('./../infrastructure/emuns/ColeccionsRef.js');

const ManageComands = class {
  constructor(clientMongo) {
    // set ready inner objects for work
    this.workComand = new Comand();
    this.managerPrices = new ManagerLogicPrices(clientMongo);
    this.clientMongo = clientMongo;
  }

  async manageTotalPriceOfComand(listItem) {
    await this.indexItemOfComand(listItem);
    // then sort the items, call to the prices calculator about their self logic price
    this.workComand.determinateComandPrize();
    return this.workComand;
  }

  // sort the list of items commanded
  async indexItemOfComand(listItmens) {
    for (const nodeList of listItmens) {
      if (typeof nodeList === 'string') {
        await this.cribeItemFromList(nodeList);
      }
    }
  }

  async cribeItemFromList(nodeItem) {
    const workIndex = this.workComand.findSelfItem(nodeItem)
    if (workIndex != -1) {
      // update the amount of items
      this.workComand.updateItemAmount(workIndex);
    } else {
      // put a new item and check their prices logic
      await this.inputNewProduct(nodeItem);
      await this.updatePricesLogic(nodeItem);
    }
  }

  async inputNewProduct(nodeList) {
    this.workComand.addNewItem(await this.findItemOnDB(nodeList));
  }

  async updatePricesLogic(nodeList) {
    this.workComand.updateItemLogic((this.workComand.items.length - 1), await this.managerPrices.findRulesPrice(nodeList));
  }

  async findItemOnDB(nodeList) {
    return await this.clientMongo.queryToCollection(ColeccionsRef.Items, {
                        code: nodeList.toUpperCase()
                      });
  }
}

module.exports = ManageComands;
