const express = require('express');
const bodyParser = require("body-parser");
const http = require('http');
const dotenv = require('dotenv');
const mongopool = require('./infrastructure/dbpool/mongopool.js');
const SetReadyDatas = require('./bussinesslogic/SetReadyDatas.js');
const factory = require('./infrastructure/services/RouterFactory.js');
const app = express();
const router = express.Router();

dotenv.config({path: '.env'});
// loat the main mongo object manager and use it on session
app.set('clientMongo', new mongopool());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use('/', factory.RouterFactory(router));
// check if the datas are ready on mongodb
SetReadyDatas.checkItemsProcess(app.get('clientMongo'));
SetReadyDatas.checkLogicProcess(app.get('clientMongo'));
module.exports = app;
